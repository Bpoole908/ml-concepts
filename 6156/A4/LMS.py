import collections # for checking iterable instance


import numpy as np
from abc import ABC, abstractmethod

# Super class for machine learning models

class BaseModel(ABC):
    """ Super class for ITCS Machine Learning Class"""

    @abstractmethod
    def train(self, X, T):
        pass

    @abstractmethod
    def use(self, X):
        pass


class LinearModel(BaseModel):
    """
        Abstract class for a linear model

        Attributes
        ==========
        w       ndarray
                weight vector/matrix
    """

    def __init__(self):
        """
            weight vector w is initialized as None
        """
        self.w = None

    def _check_matrix(self, mat, name):
        if len(mat.shape) != 2:
            raise ValueError(''.join(["Wrong matrix ", name]))

    # add a basis
    def add_ones(self, X):
        """
            add a column basis to X input matrix
        """
        self._check_matrix(X, 'X')
        return np.hstack((np.ones((X.shape[0], 1)), X))

    ####################################################
    #### abstract funcitons ############################
    @abstractmethod
    def train(self, X, T):
        """
            train linear model

            parameters
            -----------
            X     2d array
                  input data
            T     2d array
                  target labels
        """
        pass

    @abstractmethod
    def use(self, X):
        """
            apply the learned model to input X

            parameters
            ----------
            X     2d array
                  input data

        """
        pass
# LMS class
class LMS(LinearModel):
    """
        Lease Mean Squares. online learning algorithm

        attributes
        ==========
        w        nd.array
                 weight matrix
        alpha    float
                 learning rate
    """
    def __init__(self, alpha):
        LinearModel.__init__(self)
        self.alpha = alpha
        self.init_run = True

    # batch training by using train_step function
    def train(self, X, T):
        error = []
        for i in range(len(X)):
            x = X[i]
            #print(x.shape, T[i].shape)
            error.append(self.train_step(x, T[i])) # append each data's error
        return error, self.w

    # train LMS model one step
    # here the x is 1d vector
    def train_step(self, x, t):
        x = np.append([1], x).reshape(-1,1) # add one for bias
        #print(x.reshape(1,-1))
        if self.init_run: # on the first run initialize weights
            self.w = np.zeros(x.shape[0]).reshape(-1,1) # set weights to 0
            self.init_run = False # indicate it is no longer the first run
        #print(x.T.shape, self.w.shape)
        y =  x.T @ self.w # make a prediction
        #print(y.shape, t.shape, x.shape)
        self.w -= self.alpha * (y - t) * x # update weights
        error = np.sqrt(np.sum((y-t)**2)) # determine train error
        return error

    # apply the current model to data X
    def use(self, X):
        X1 = self.add_ones(X) # add ones for bias
        Y = X1 @ self.w # make a prediction
        return Y
