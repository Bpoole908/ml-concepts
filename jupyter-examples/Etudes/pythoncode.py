import numpy
def append_if_not_exists(arr, x):
    if x not in arr:
        arr.append(x)
        
def some_useless_slow_function(n=5000):
    arr = list()
    for i in range(n):
        x = numpy.random.randint(0, n)
        append_if_not_exists(arr, x)
