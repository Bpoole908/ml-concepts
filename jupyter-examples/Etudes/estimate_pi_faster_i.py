import numpy as np
def estimate_pi_2(n=10000000):
    """Estimate pi with monte carlo simulation.
    
    Arguments:
        n: number of simulations
    """
    xy = np.random.rand(n, 2)
    inside = np.sum(xy[:, 0]**2 + xy[:, 1]**2 <= 1)
    return 4 * inside / n

estimate_pi_faster()
