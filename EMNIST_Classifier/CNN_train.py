import tensorflow as tf
import CNN_Arch as cnn

def Train_CNN(X, Y, X_Test, Y_Test, n_epochs, batch_size, path, load_model):
    # Tensors
    x = tf.placeholder('float', [None, X.shape[1]]) 
    y = tf.placeholder('float')
    keep_prob = tf.placeholder('float')

    # Variables
    pred = cnn.CNN(x, keep_prob)
    acc = 0
    
    with tf.name_scope("loss"):
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=pred))
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
        
    with tf.name_scope("precision"):
        correct = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
        
    saver = tf.train.Saver()
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        init.run()
        #saver.restore(sess, load_model) # First time running comment out
        
        # Batch Train
        for epoch in range(n_epochs):
            epoch_loss = 0
            for batch in range (int(len(X)/batch_size)):
                index = batch + 1
                epoch_x = X[batch*batch_size:batch_size*index,:]
                epoch_y = Y[batch*batch_size:batch_size*index,:]
                _, l = sess.run([optimizer, loss], feed_dict = {x: epoch_x, y: epoch_y, keep_prob : .8})
                epoch_loss += l
            print('Epoch', epoch, "loss:", epoch_loss, "accuracy", 
                accuracy.eval({x:epoch_x, y:epoch_y, keep_prob : 1.0})  * 100.0)
            
        # Batch Test
        batch_total = int(len(X_Test)/batch_size)
        for batch in range (batch_total):
            index = batch + 1
            batch_x = X_Test[batch*batch_size:batch_size*index,:]
            batch_y = Y_Test[batch*batch_size:batch_size*index,:]
            #print(accuracy.eval({x:epoch_x, y:epoch_y, keep_prob : 1.0}))
            acc += accuracy.eval({x:batch_x, y:batch_y, keep_prob : 1.0})
        acc_total = (acc/batch_total)
        print('Batch Average Accuracy', (acc/batch_total) * 100.0)
        save_path = saver.save(sess, path) # Path specific to computer

