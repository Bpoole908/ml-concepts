import pandas as pd
import numpy as np
import CNN_train as train

def load_dataset(train, test):
    #Data Loading with target in first column
    header = range(0, 785)
    train_data = pd.read_csv(train, names=header)
    target_data = train_data[0]
    train_data = train_data.drop(0, axis=1)

    test_data = pd.read_csv(test, names=header)
    target_test_data = test_data[0]
    test_data = test_data.drop(0, axis=1)
    print('Done Loading')
    return train_data, target_data, test_data, target_test_data

# Variables
path = r'tmp\model.ckpt'
load_model = r'tmp\model.ckpt' 
emnist_data, emnist_target, emnist_data_test, emnist_target_test = load_dataset('emnist-balanced-train-flipped.csv', 'emnist-balanced-test-flipped.csv')
X = np.asmatrix(emnist_data.values)
Y = pd.get_dummies(emnist_target).values
X_Test = np.asmatrix(emnist_data_test.values)
Y_Test = pd.get_dummies(emnist_target_test).values

# Shapes
print('Train Shape:', X.shape)
print('Train Target Shape:', Y.shape)
print('Test Shape:', X_Test.shape)
print('Test Target Shape:', Y_Test.shape)

train.Train_CNN(X=X, Y=Y, X_Test=X_Test, Y_Test=Y_Test, n_epochs=15, batch_size=512, path=path, load_model=load_model)