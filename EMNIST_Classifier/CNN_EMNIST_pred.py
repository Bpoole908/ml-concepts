import pandas as pd
import numpy as np
import tensorflow as tf
import CNN_pred as prediction

# code for loading matrix from drawing
def load_sample():
    header = range(0, 784)
    sample = pd.read_csv('sample.txt', delimiter=',', names=header)
    sample = np.asmatrix(sample)
    sample = sample.reshape(1, 784)
    print('shape', sample.shape)
    return sample
    
def load_sample2():
    nrows = 3
    drop_rows = np.arange(0, nrows-1)
    header = range(0, 785)
    sample = pd.read_csv('emnist-bymerge-test.csv', delimiter=',', nrows=nrows, names=header)
    sample = sample.drop(drop_rows, axis=0)
    sample = sample.drop(0, axis=1)
    print('shape', sample.shape)
    return sample

target_dic = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'A', 11:'B', 12:'C', 13:'D', 14:'E', 15:'F', 16:'G', 17:'H', 18:'I', 19:'J', 20:'K', 21:'L', 22:'M', 23:'N', 24:'O',
25:'P', 26:'Q', 27:'R', 28:'S', 29:'T', 30:'U', 31:'V', 32:'W', 33:'X', 34:'Y', 35:'Z', 36:'a', 37:'b', 38:'d', 39:'e', 40:'f', 41:'g', 42:'h', 43:'n', 44:'q', 45:'r', 46:'t'}
#load_model = r'tmp\model.ckpt'
load_model = r'tmp\models\512_001_10k_.8\model.ckpt'
sample = load_sample()

tf.reset_default_graph()
pred = prediction.CNN_pred(sample=sample, model=load_model)
print(pred)
print('Prediction', target_dic[np.argmax(pred)])
print('Prediction Index', np.argmax(pred))
print('length of pred', len(pred))