import tensorflow as tf

n_classes = 47 # A-Z, a-z (duplicates matched with capitals), 0-9
hidden = 10000
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def maxpool2d(x):
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

def CNN(x, keep_prob):
    weights = {
        'W_conv1' : tf.Variable(tf.random_normal([5,5,1,32])),
        'W_conv2' : tf.Variable(tf.random_normal([5,5,32,64])),
        'W_fc' : tf.Variable(tf.random_normal([7*7*64, hidden])),
        'out' : tf.Variable(tf.random_normal([hidden, n_classes]))}
    biases = {
        'b_conv1' : tf.Variable(tf.random_normal([32])),
        'b_conv2' : tf.Variable(tf.random_normal([64])),
        'b_fc' : tf.Variable(tf.random_normal([hidden])),
        'out' : tf.Variable(tf.random_normal([n_classes]))}
    
    #reshape inputs to samples x 28 x 28 x color channels
    x = tf.reshape(x, shape=[-1, 28, 28, 1])
    
    # Conv1 layer
    conv1 = conv2d(x, weights['W_conv1']) + biases['b_conv1']
    conv1 = tf.nn.relu(conv1)
    conv1 = maxpool2d(conv1)
    
    # Conv2 layer
    conv2 = conv2d(conv1, weights['W_conv2']) + biases['b_conv2']
    conv2 = tf.nn.relu(conv2)
    conv2 = maxpool2d(conv2)
    
    # Fully connected layer
    fc = tf.reshape(conv2, [-1, 7*7*64])
    fc = tf.matmul(fc, weights['W_fc']) + biases['b_fc']
    fc = tf.nn.relu(fc)
    fc = tf.nn.dropout(fc, keep_prob)
    output = tf.matmul(fc, weights['out']) + biases['out']
    
    return output