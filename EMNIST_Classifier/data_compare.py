import tensorflow as tf
import numpy as np
import pandas as pd

#Data Loading
header = range(0, 785)
emnist_data_test = pd.read_csv('emnist-balanced-test.csv', nrows=21,names=header)
emnist_target_test = emnist_data_test[0]
emnist_data_test = emnist_data_test.drop(0, axis=1)

print('Done Loading')

header = range(0, 784)
sample = pd.read_csv('sample.txt', delimiter=',', names=header)
sample = np.asmatrix(sample)
sample.shape

# Variables
X_Test = np.asmatrix(emnist_data_test.values)
Y_Test = pd.get_dummies(emnist_target_test).values

# Shapes
print('Test Shape:', X_Test.shape)
print('Test Target Shape:', Y_Test.shape)
loc = 18


compare = X_Test[loc,:].reshape(28,28)

flipped = np.fliplr(compare)
compare = np.rot90(flipped)

print("SAMPLE", sample.reshape(28,28))
print("TEST DATA", compare)

np.savetxt('compare.txt', compare, delimiter=',', fmt='%.f') 
