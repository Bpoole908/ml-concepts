import tensorflow as tf
import CNN_Arch as cnn

# Prediction framework for drawing
def CNN_pred(sample, model):
    x = tf.placeholder('float', [None, sample.shape[1]]) 
    keep_prob = tf.placeholder('float')
    pred = cnn.CNN(x, keep_prob)
    pred_sample = tf.nn.softmax(logits=pred)

    saver = tf.train.Saver()
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        init.run()
        saver.restore(sess, model) # load model
        feed_dict = { x : sample, keep_prob : 1}
        predictions = sess.run([pred_sample], feed_dict)
    return predictions